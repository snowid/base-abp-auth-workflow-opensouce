import request from '@/utils/request'

export function getOrganizationUnits() {
  return request({
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnits',
    method: 'get'
  })
}
export function getOrganizationUnitUsers(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitUsers',
    method: 'get',
    params: data
  })
}

export function createOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/createOrganizationUnit',
    method: 'post',
    data
  })
}

export function updateOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/UpdateOrganizationUnit',
    method: 'put',
    data
  })
}

export function moveOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/MoveOrganizationUnit',
    method: 'post',
    data
  })
}

export function deleteOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/DeleteOrganizationUnit',
    method: 'delete',
    params: data
  })
}

export function removeUserFromOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/RemoveUserFromOrganizationUnit',
    method: 'delete',
    params: data
  })
}

export function addUsersToOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/AddUsersToOrganizationUnit',
    method: 'post',
    data
  })
}

export function findUsers(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/FindUsers',
    method: 'post',
    data
  })
}
