import request from '@/utils/request'

export function getUserNotifications(data) {
  return request({
    url: 'api/services/app/Notification/GetUserNotifications',
    method: 'get',
    params: data
  })
}

export function getNotificationSettings() {
  return request({
    url: 'api/services/app/Notification/GetNotificationSettings',
    method: 'get'
  })
}

export function setNotificationAsRead(data) {
  return request({
    url: 'api/services/app/Notification/SetNotificationAsRead',
    method: 'post',
    data
  })
}

export function setAllNotificationsAsRead() {
  return request({
    url: 'api/services/app/Notification/SetAllNotificationsAsRead',
    method: 'post'
  })
}

export function updateNotificationSettings(data) {
  return request({
    url: 'api/services/app/Notification/UpdateNotificationSettings',
    method: 'put',
    data
  })
}

export function deleteNotification(Id) {
  return request({
    url: 'api/services/app/Notification/DeleteNotification',
    method: 'delete',
    params: { Id }
  })
}
