﻿import crud from './common/crudCodeBulid'

const advanceTask = {
  namespaced: true,
  state: {
    ...crud.state
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions
  }
}

export default advanceTask
