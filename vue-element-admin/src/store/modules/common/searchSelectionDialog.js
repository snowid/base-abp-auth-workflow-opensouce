import request from '@/utils/request'

const state = {
  entityName: '',
  searchLoading: false, // 控制非dialog界面的searchLoading
  searchList: [],
  searchTotalCount: 0,
  searchPageSize: 10,
  searchCurrentPage: 1
}

const mutations = {
  SET_INIT: (state, entityName) => {
    state.entityName = entityName
  },
  SET_PAGE_SIZE(state, size) {
    state.searchPageSize = size
  },
  SET_CURRENT_PAGE(state, page) {
    state.searchCurrentPage = page
  }
}

const actions = {
  getSearchSelectionAll({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.searchLoading = true
      request({
        url: `/api/services/app/${state.entityName}/getSearchSelectionAll`,
        method: `post`,
        data: payload.data
      })
        .then(response => {
          state.searchList = []
          state.searchList.push(...response.data.result.items)
          state.searchTotalCount = response.data.result.totalCount
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.searchLoading = false
        })
    })
  }
}

export default { state, mutations, actions }
